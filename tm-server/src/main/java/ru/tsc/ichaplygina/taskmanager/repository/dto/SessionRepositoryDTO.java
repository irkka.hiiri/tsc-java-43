package ru.tsc.ichaplygina.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.ISessionRepositoryDTO;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepositoryDTO extends AbstractRepositoryDTO<SessionDTO> implements ISessionRepositoryDTO {

    public SessionRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM SessionDTO e").executeUpdate();
    }

    @Override
    public @NotNull List<SessionDTO> findAll() {
        return entityManager.createQuery("FROM SessionDTO", SessionDTO.class).getResultList();
    }

    @Override
    public @Nullable SessionDTO findById(@NotNull final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(1) FROM SessionDTO e", Long.class).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(SessionDTO.class, id));
    }
}
