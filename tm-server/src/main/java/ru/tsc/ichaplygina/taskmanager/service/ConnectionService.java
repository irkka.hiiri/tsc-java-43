package ru.tsc.ichaplygina.taskmanager.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.property.IDatabaseProperty;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;
import ru.tsc.ichaplygina.taskmanager.dto.UserDTO;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Session;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperties;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;


    public ConnectionService(@NotNull IDatabaseProperty databaseProperties) {
        this.databaseProperties = databaseProperties;
        entityManagerFactory = factory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, databaseProperties.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, databaseProperties.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, databaseProperties.getDatabaseUsername());
        settings.put(org.hibernate.cfg.Environment.PASS, databaseProperties.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseProperties.getDatabaseSqlDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseProperties.getDatabaseHbm2ddlAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseProperties.getDatabaseShowSql());
        @NotNull final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }


}
