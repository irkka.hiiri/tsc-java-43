package ru.tsc.ichaplygina.taskmanager.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;

public interface IProjectServiceDTO extends IAbstractBusinessEntityServiceDTO<ProjectDTO> {


}
